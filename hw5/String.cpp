#include "String.h"

// String --------------------------------------
String::String(const char *s)
    :head(ListNode::stringToList(s)){}

String::String(const String &s)
    :head(ListNode::copy(s.head)){}

String::~String(){
    ListNode::deleteList(head);
}

String String::operator = (const String &s){
    ListNode::deleteList(this->head);
    this->head = ListNode::copy(s.head);
    return *this;
}

char & String::operator [] (const int index){
    if(!inBounds(index)){
        std::cerr << "Error: Index " << index << " not in bounds" << std::endl;
    }
    ListNode *temp = head;
    for(int i = 0; i < index && temp->next; ++i ){
        temp = temp->next;
    }
    return temp->info;
}


int String::indexOf(char c) const{
    ListNode *temp = head;
    int i = 0;
    for(i = 0; temp && temp->info != c;i++){
        temp = temp->next;
    }
    return i;
}

bool String::operator == (const String &s) const{
    return ListNode::equal(this->head,s.head);
}

bool String::operator < (const String &s) const{
    return ListNode::compare(this->head,s.head) < 0;
}

String String::operator + (const String &s){
    String m(*this);
    m.head = ListNode::concat(m.head,s.head);
    return m;
}
String String::operator += (const String &s){
    this->head = ListNode::concat(this->head,s.head);
    return *this;
}

void String::print(std::ostream &out){
    ListNode *temp = head;
    while(temp){
        out << temp->info;
        temp = temp->next;
    }
}

void String::read(std::istream &in){
    char c[2];
    ListNode::deleteList(head);
    ListNode *temp = new ListNode('a',nullptr);
    head = temp;
    while(in.get(c,2,'\n')){
        temp->next = new ListNode(c[0],nullptr);
        temp = temp->next;
    }
    temp = head;
    head = head->next;
    delete temp;
}

// ListNode -------------------------------------
String::ListNode * String::ListNode::stringToList(const char *s){
    ListNode *node = nullptr;
    if(*s){
        node = new ListNode(*s,stringToList(s+1));
    }
    return node;
}

String::ListNode * String::ListNode::copy(ListNode *L){
    ListNode *node = nullptr;
    if(L){
        node = new ListNode(L->info,copy(L->next));
    }
    return node;
}

bool String::ListNode::equal(ListNode *L1, ListNode *L2){
    if(L1 && L2){
        if(L1->info == L2->info){
            if(L1->next && L2->next){
                return equal(L1->next,L2->next);
            } else {
                return true;
            }
        } else {
            return false;
        }
    } else {
        return false;
    }
}

String::ListNode * String::ListNode::concat(ListNode *L1,ListNode *L2){
    ListNode *left = L1;
    while(( left = left->next )->next)
        ;
    left->next = copy(L2);
    return L1;
}

int String::ListNode::compare(ListNode *L1, ListNode *L2){
    if(L1 && L2){
        if(L1->info != L2->info){
            return L1->info - L2->info;
        } else if (L1->next && L2->next){
            return compare(L1->next,L2->next);
        } else {
            return 0;
        }
    } else if(L1){
        return L1->info;
    } else {
        return L2->info;
    }
}

void String::ListNode::deleteList(ListNode *L){
    if(L){
        deleteList(L->next);
        delete L;
    }
}

int String::ListNode::length(ListNode *L){
    if(L){
        return 1+length(L->next);
    } else {
        return 0;
    }
}

std::ostream & operator << (std::ostream &out, String str){
    str.print(out);
    return out;
}
std::istream & operator >> (std::istream &in, String &str){
    str.read(in);
    return in;
}
