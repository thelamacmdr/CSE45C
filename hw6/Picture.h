#ifndef PICTURE_H
#define PICTURE_H
#include "Shape.h"

class Picture {

    private:
        
        struct Node {
            public:
                Node(Shape *s,Node *next)
                    :shape(s),next(next){}

                ~Node(){
                    delete next;
                }
                Shape *shape;
                Node *next;
        };
        Node *head;


    public:
        Picture()
        :head(nullptr){}

        void add(Shape *sp){
            Node *temp = new Node(sp,head);
            head = temp;
        }

        void drawAll(){
            Node *temp = head;
            while(temp){
                temp->shape->draw();
                temp = temp->next;
            }
        }

        double totalArea(){
            double area = 0.0;
            Node *temp = head;
            while(temp){
                area += temp->shape->area();
                temp = temp->next;
            }
            return area;
        }

        ~Picture(){
            delete this->head;
        }

};

#endif
