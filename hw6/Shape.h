#ifndef STRING_H
#define STRING_H
#include <string>
using namespace std;

class Shape {
    public:
        Shape(string name,int x, int y)
            :name(name)
             ,x(x)
             ,y(y){};
        string name;
        virtual double area() = 0;
        virtual void draw() = 0;

        virtual ~Shape(){}
    
    protected:
        int x;
        int y;

};
#endif
