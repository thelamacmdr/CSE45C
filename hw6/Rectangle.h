#ifndef RECTANGLE_H
#define RECTANGLE_H
#include "Shape.h"
#include <iostream>
using namespace std;


class Rectangle : public Shape{
    public:
        Rectangle(int x, int y, int w, int h)
            :Shape(name,x,y)
             ,width(w)
             ,height(h){}

        double area(){
            return (double) height * width;
        }

        void draw(){
            cout <<"*******\n*     *\n*     *\n*     *\n*     *\n*     *\n*     *\n*******\n" << endl;
        }

    private:
        const string name = "Rectangle";
        int width;
        int height;
};
#endif
