#include "String.h"
#include<iostream>
#define INITIAL_SIZE 4


// Public member functions
String::String(const char *s)
:buf(strdup(s)){
}

String::String(const String &s)
:buf(strdup(s.buf)){
}

String String::operator = (const String &s){
    if(strlen(buf) < strlen(s.buf)){
        resize(buf,strlen(s.buf));
    }
    strcpy(buf,s.buf);
    return *this;
}

char& String::operator[](int index){
    if(!inBounds(index)){
        std::cerr << "Error: Index " << index << " is out of bounds" << std::endl;
    }
    return buf[index];
}

String String::reverse() {// does not mutate
    int len = strlen(buf)-1;
    char temp[len+1];
    int i;
    for (i = 0;buf[i] != '\0';++i){
        temp[i] = buf[len - i];
    }
    temp[i] = '\0';
    return String(temp);
}

int String::size(){
    return strlen(buf);
}
int String::indexOf(const char c){
    return size() - strlen(strchr(this->buf,c));
}


int String::indexOf(const String pattern){
    return size() - strlen(strstr(this->buf,pattern.buf));
}
bool String::operator == (const String s){
    return (strcmp(buf,s.buf) == 0);
}
bool String::operator != (const String s){
    return (strcmp(buf,s.buf) != 0);
}
bool String::operator > (const String s){
    return (strcmp(buf,s.buf) > 0);
}
bool String::operator < (const String s){
    return (strcmp(buf,s.buf) < 0);
}
bool String::operator <= (const String s){
    return (strcmp(buf,s.buf) <= 0);
}
bool String::operator >= (const String s){
    return (strcmp(buf,s.buf) >= 0);
}

String String::operator +(const String s){
    String m(buf);
    strcat(m.buf,s.buf);
    return m;
}

String String::operator +=(const String s){
    strcat(buf,s.buf);
    return *this;
}

void String::print(std::ostream &out){
    out << buf;
}

void String::read(std::istream &in){
    int buffer = 48;
    int size = buffer;
    delete_char_array(buf);
    buf = new_char_array(size);
    char *temp = new_char_array(buffer);
    while(in.get(temp,buffer,'\n')){
        strcat(buf,temp);
        size = size+buffer;
        resize(buf,size);
    }
    delete_char_array(temp);
}

void String::print_allocations(){
    std::cout << "Number of new allocations minus number of delete deallocations is " << allocations << std::endl;
}

String::~String(){
    delete_char_array(buf);
}


// Private member functions
//
int String::allocations = 0;

bool String::inBounds(int i){
    return i >= 0 && i < strlen(buf);
}


int String::strlen(const char *src){
    int len = 0;
    while(src && *src){
        ++len;
        ++src;
    }
    return len;
}

char* String::strchr(const char *str, int c){
    for(int i = 0; str[i] != '\0'; i++){
        if(c == str[i]){
            return (char*)&str[i];
        }
    }
    return nullptr;
}

char* String::strstr(const char* haystack, const char* needle){
    char *find = strchr(haystack,needle[0]);
    while(find && strncmp(find,needle,strlen(needle))){
        find++;
        find = strchr(find,needle[0]);
    }
    return find;
}

char* String::strcat(char* &dest, const char* src){
    int i = strlen(dest);
    int m;
    resize(dest,i+strlen(src));
    for(m = 0; src[m] != '\0';m++){
        dest[i+m] = src[m];
    }
    dest[i+m] = '\0';
    return dest;
}

char* String::resize(char *&dest, int size){
    char *temp = dest;
    dest = new_char_array(size+1);
    strcpy(dest,temp);
    delete_char_array(temp);
    return dest;
}

char* String::strdup(const char *src){
    char *temp = new_char_array(strlen(src)+1);
    strcpy(temp,src);
    return temp;
}

char* String::strcpy(char *dest, const char *src){
    int i;
    for(i = 0;src[i] != '\0';i++){
        dest[i] = src[i];
    }
    dest[i] = '\0';
    return dest;
}

int String::strcmp( const char *left, const char *right){
    while(*left && (*left == *right)){
        ++left;
        ++right;
    }
    return *left - *right;
}

int String::strncmp(const char *left, const char *right, int n){
    int i = 1;
    while(*left && (*left == *right) && i <= n){
        left++;
        right++;
        i++;
    }
    return *left - *right;
}

char* String::new_char_array(int n_bytes){
    ++allocations;
    return new char[n_bytes];
}

void String::delete_char_array(char *p){
    --allocations;
    if(allocations >= 0){
        delete[] p;
    } else {
        std::cout << "Can't deallocate anymore" << std::endl;
    }
}

std::ostream & operator << (std::ostream &out, String str){
    str.print(out);
    return out;
}
std::istream & operator >> (std::istream &in, String &str){
    str.read(in);
    return in;
}
