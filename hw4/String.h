#include <iostream> 
class String {
    public:
        String(const char *s = "");
        String(const String &s);
        String operator = (const String &s);
        char & operator [] (int index);
        int size();
        String reverse();
        int indexOf(const char c);
        int indexOf(const String pattern);

        bool operator == (const String s);
        bool operator != (const String s);
        bool operator > (const String s);
        bool operator < (const String s);
        bool operator >= (const String s);
        bool operator <= (const String s);

        String operator + (const String s);
        String operator += (const String s);
        void print(std::ostream &out);
        void read(std::istream &in);
        ~String();
        static void print_allocations();
    private:
        char *buf;
        static int allocations;
        bool inBounds(int i);
        static void setAllocations();
        static int strlen(const char *src);
        static char* strchr(const char *str, int c);
        static char* strstr(const char *haystack, const char *needle);
        static char* strcat(char* &dest, const char * src);
        static char* resize(char *&dest, int size);
        static char* strdup(const char *src);
        static int strcmp(const char *left, const char *right);
        static int strncmp(const char *left, const char*right, int n);
        static char* strcpy(char *dest, const char *src);

        static char* new_char_array(int n_bytes);

        static void delete_char_array(char *p);
};

std::ostream & operator << (std::ostream &out, String str);
std::istream & operator >> (std::istream &in, String &str);
