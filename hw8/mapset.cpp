#include <iostream>
#include <fstream>
#include <iterator>
#include <string>
#include <map>
#include <set>
#include <functional>
#include <algorithm>
using namespace std;

int main(){
    ifstream in("sample_doc.txt");
    ifstream ex("stopwords.txt");
    ofstream out("frequency.txt");

    map<string,int> inputMap;
    set<string> exclusionList;

    // Place stopwords.txt into exclusionList
    copy(istream_iterator<string>(ex),istream_iterator<string>(),inserter(exclusionList,exclusionList.end()));

    // Count items in sample_doc except for those in the exclusionList
    for_each(istream_iterator<string>(in),istream_iterator<string>(),[&](string s){
            
            transform(begin(s),end(s),begin(s),::tolower);
         
            if(exclusionList.find(s) == exclusionList.end() )
                inputMap[s]++;
            });

    // Output sample_doc map
    for_each(begin(inputMap),end(inputMap), [&](pair<string,int> m){
            cout << m.first << " " << m.second << endl;
            out << m.first << " " << m.second << endl;
            } );

    return 0;
}


