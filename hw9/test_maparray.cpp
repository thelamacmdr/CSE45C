#include <iostream>
#include <fstream>
#include <iterator>
#include <string>
#include <functional>
#include <algorithm>
#include "MapArray.h"
using namespace std;

int main(){
    MapArray<string,int> inputMap;
    ifstream in("sample_doc.txt");

    for_each(istream_iterator<string>(in),istream_iterator<string>(),[&](string s){
            inputMap[s]++;
            });

    for_each(begin(inputMap),end(inputMap),[&](pair<string,int> m){
            cout << m.first << " " << m.second << endl;
            });
}
