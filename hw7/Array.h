#ifndef ARRAY_H
#define ARRAY_H

#include<iomanip>
#include<iostream>
using namespace std;


template<typename T>
class Array {
    private:
        int len;
        T *buf;
        // Bound checking stuff
        virtual bool inBounds(int i){
            return (i >= 0 && i < len);
            
        }
    public:
        class IndexOutOfBoundsException{};

        Array(int newLen)
            :len(newLen),buf(new T[newLen])
        {}

        Array(const Array &l)
            :len(l.len),buf(new T[l.len])
        {
            for(int i = 0; i<l.len;i++){
                buf[i] = l.buf[i];
            }
        }

        T &operator [] (int i){
            if(!inBounds(i)){
                throw IndexOutOfBoundsException();
            }
            return buf[i];
        }

        void print(ostream &out){
            for(int i = 0; i < len; i++){
                out << setw(8) << setprecision(2) << fixed << right << buf[i];
            }
        }

        friend ostream & operator << (ostream &out, Array &a){
            a.print(out);
            return out;
        }

        friend ostream &operator << (ostream &out, Array *ap){
            ap->print(out);
            return out;
        }


};

#endif
