#include <iostream>
#define MAXLEN 128

class String {
    public:
        String(const char *s = ""){
            strcpy(buf,s);
        }
        String(const String &s){
            strcpy(buf,s.buf);
        }
        String operator = (const String &s){
            strcpy(buf,s.buf);
            return *this;
        }
        
        char &operator [] (int index){
            return buf[index];
        }

        int size(){
            return strlen(buf);
        }

        String reverse() {// does not mutate
            char temp[MAXLEN];
            int len = strlen(buf)-1;
            temp[0] = buf[len];
            for (int i = 1;buf[i-1] != '\0';i++){
                temp[i] = buf[len - i];
            }
            return String(temp);
        }
        int indexOf(const char c){
            return size() - strlen(strchr(this->buf,c));
        }

        int indexOf(const String pattern){
            return size() - strlen(strstr(this->buf,pattern.buf));
        }
        bool operator == (const String s){
            return (strcmp(buf,s.buf) == 0);
        }
        bool operator != (const String s){
            return (strcmp(buf,s.buf) != 0);
        }
        bool operator > (const String s){
            return (strcmp(buf,s.buf) > 0);
        }
        bool operator < (const String s){
            return (strcmp(buf,s.buf) < 0);
        }
        bool operator <= (const String s){
            return (strcmp(buf,s.buf) <= 0);
        }
        bool operator >= (const String s){
            return (strcmp(buf,s.buf) >= 0);
        }
        String operator + (const String s){
            String m(*this);
            strcat(m.buf,s.buf);
            return m;
        }
        String operator += (const String s){
            strcat(this->buf,s.buf);
            return *this;
        }
        void print(std::ostream &out){
            out << buf;
        }
        void read(std::istream &in){
            in.getline(this->buf,MAXLEN);
        }
        ~String(){
        }
    private:
        bool inBounds( int i){
            return i >= 0 && i < strlen(buf);
        }
        static int strlen(const char *s){
            int len = 0;
            while(s && s[len] != '\0'){
                len++;
            }
            return len;
        }
        static char *strcpy(char *dest, const char *src){
            dest[0] = src[0];
            for(int i = 1; src[i-1] != '\0';i++){
                dest[i] = src[i];
            }
            return dest;
        }
        static char *strcat(char *dest, const char *src){
            int i = strlen(dest);
            dest[i] = src[0];
            for(int m = 1; src[m-1] != '\0';m++){
                dest[i+m] = src[m];
            }
            return dest;
        }

        static int strcmp( const char *left, const char *right){
            for(int i = 0;right[i] != '\0';i++){
                if(left[i] == '\0' || left[i] != right[i]){
                    return left[i] - right[i];
                }            
            }
            return 0;
        }
        static int strncmp(const char *left, const char *right, int n){
            for(int i = 0;i < n && right[i] != '\0';i++){
                if(left[i] == '\0' || left[i] != right[i]){
                    return left[i] - right[i];
                }
            }
            return 0;
        }
        static char *strchr( const char *str, int c){
            for(int i = 0; str[i] != '\0'; i++){
                if(c == str[i]){
                    return (char*)&str[i];
                }
            }
            return '\0';
        }
        static char *strstr(const char *haystack, const char *needle){
            char *find = strchr(haystack,needle[0]);
            while(find && strncmp(find,needle,strlen(needle))){
                find++;
                find = strchr(find,needle[0]);
            }
            return find;
        }
        char buf[MAXLEN];// array for chars in string

};

std::ostream & operator << (std::ostream &out, String str){
    str.print(out);
    return out;
}
std::istream & operator >> (std::istream &in, String &str){
    str.read(in);
    return in;
}
