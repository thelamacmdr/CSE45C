URL=anthonml@odin.ics.uci.edu
FOLDER=$(basename "$(pwd)")
CXXFLAGS=-Wall -ggdb -std=c++11

default: 
	@echo "Compilation successful"

rule:
	$(CXX) $(CXXFLAGS) $? -o bin/

copy:
	scp *.cpp *.h Makefile $(URL):~/$(FOLDER)

remote: copy
	ssh $(URL) "mkdir $(FOLDER) && cd $(FOLDER) && mkdir bin"
